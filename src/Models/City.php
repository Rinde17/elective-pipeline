<?php

namespace src\Models;

class City
{

    private $name;
    public function __construct($name = "")
    {
        $this->name = $name;
    }

    public function getCityNameById($id)
    {
        if ($id == 1) {
            $city = "Bordeaux";
        } else {
            $city = "Paris";
        }
        return $city;
    }

}
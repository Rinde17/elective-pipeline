<?php

use PHPUnit\Framework\TestCase;
use src\Models\City;
require __DIR__ . "/../src/Models/City.php";

class CityTest extends TestCase
{
    public function testgetCityNameById()
    {
        $city = new City();
        $result = $city->getCityNameById(1);
        $expected = 'Bordeaux';
        $this->assertTrue($result == $expected);
    }
}
